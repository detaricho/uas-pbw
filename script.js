const hasil = document.getElementById('isi');
const button = document.getElementById('button');
const no_meja = document.getElementById('no_meja');
const menu_pesanan = document.getElementById('menu_pesanan');
const qty = document.getElementById('qty');

let idpemesanan = 1;
let pemesanan = [];

dataDummy();
showData();

function prosesSimpanEdit(){
    if (button.innerHTML === "Simpan") {
        validasiInput()
     
    }else{
        validasiEdit()

    }
        
}


function validasiInput(){
    

   if (no_meja.value == "" || menu_pesanan.value == "" || qty.value == "") {
          alert("Data Harus diisi secara lengkap!!");

        }else{

        if (qty.value < 0) {
            alert("qty tidak boleh bernilai minus!!");
          }else{


       var index = pemesanan.map(function(e) { return e.no_meja; }).indexOf(no_meja.value);
        if (index >= 0) {
            if (pemesanan[index].no_meja == no_meja.value) {
               alert("no_meja pemesanan sudah ada !!")
            }

        }else{
          input()   
          alert("Data Berhasil diinput")
        }
            

           }
        
            
    
        }
}

function validasiEdit(){
    if (no_meja.value == "" || menu_pesanan.value == "" || qty.value == "") {
          alert("Data Harus diisi secara lengkap!!");
        }else{

        if (qty.value < 0) {
            alert("qty tidak boleh bernilai minus!!");
          }else{

            var index = pemesanan.map(function(e) { return e.no_meja; }).indexOf(no_meja.value);
        if (index >= 0) {
            if (pemesanan[index].no_meja == no_meja.value) {
               updateData(button.getAttribute("data-type"));
            }

        }else{
          updateData(button.getAttribute("data-type")); 
                alert("Data berhasil diedit !!") 
        }


          }
                
    
        }
}

function input(){
    let fruit = {
        id : idpemesanan,
        no_meja : no_meja.value,
        menu_pesanan : menu_pesanan.value,
        qty : qty.value
    }

    pemesanan.push(fruit);
    idpemesanan++;

    no_meja.value = "";
    menu_pesanan.value = "";
    qty.value = "";

    showData();

}

function showData(){
    hasil.innerHTML = '';

    pemesanan.forEach(pemesananku => {
        hasil.innerHTML += `
          <tr>
              <td>${pemesananku.id}</td>
              <td>${pemesananku.no_meja}</td>
              <td>${pemesananku.menu_pesanan}</td>
              <td>${pemesananku.qty}</td>
              <td align="center">
                 <button type="button" class="buttonDelete" onclick="deleteClick(${pemesananku.id})">Delete</button>
                 <button type="button" onclick="editClick(${pemesananku.id})">Edit</button>
              </td>
          </tr>
        `
    })

     no_mejapemesanan.innerHTML = '';
     pemesanan.forEach(dataku => {
        no_mejapemesanan.innerHTML += `
          <option value="${dataku.no_meja}">${dataku.no_meja}</option>
        `
    })

}

function deleteClick(id){
      const pemesananku = pemesanan.find(function(pemesananku){
        return pemesananku.id === id
      })

      if (confirm(`Apakah anda ingin menghapus data ${pemesananku.no_meja} ?`)) {
          pemesanan = pemesanan.filter(function(pemesananku){
            return pemesananku.id != id
          })
      }

      showData();
}

function editClick(id){
    const pemesananku = pemesanan.findIndex(pemesananku =>{
        return pemesananku.id === id
    })

    no_meja.value = pemesanan[pemesananku].no_meja;
    menu_pesanan.value = pemesanan[pemesananku].menu_pesanan;
    qty.value = pemesanan[pemesananku].qty;
    no_meja.disabled = true;

    button.innerHTML = "Update";
    button.setAttribute("data-type", pemesananku);
}

function updateData(id){

    let f = {
        id : pemesanan[id].id,
        no_meja : no_meja. value,
        menu_pesanan : menu_pesanan.value,
        qty : qty.value
    }

    pemesanan.splice(id, 1, f);
    Reset();
    showData();

    no_meja.value = "";
    menu_pesanan.value = "";
    qty.value="";
    no_meja.disabled = false;
    button.removeAttribute("data-type");
    button.innerHTML = "Simpan";
}

function dataDummy(){
   
    let pesanan = {
        id: idpemesanan,
        no_meja: "01",
        menu_pesanan : "Nasi Ayam Bali",
        qty : 3
    };

    pemesanan.push(pesanan);
     idpemesanan++; 

    let pesanan2 = {
        id: idpemesanan,
        no_meja: "02",
        menu_pesanan : "Orak Arik Telur",
        qty : 2
    };

    pemesanan.push(pesanan2);

    idpemesanan++;

}

function Reset(){
    no_meja.value = "";
    menu_pesanan.value = "";
    qty.value="";
    no_meja.disabled = false;
    button.removeAttribute("data-type");
    button.innerHTML = "Simpan";
}